package controllers;

import models.Tag;
import play.data.Form;
import java.util.List;
import java.util.*;
import models.Product;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.details;
import views.html.products.list;
import java.util.ArrayList;

public class Products extends Controller{
	private static final Form<Product> productForm = Form.form(Product.class);
	
	public static Result list(){
		List<Product> products = Product.findAll();
		return ok(list.render(products));
	}
	
	public static Result newProduct() {
		return ok(details.render(productForm));
	}
	
	public static Result details(Product product) {
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	
	public static Result save() {
		Form<Product> boundForm = productForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		}
		Product product = boundForm.get();
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}
		}
		product.tags = tags;
		product.save();
		flash("success", String.format("Successfully added product %s", product));
		return redirect(routes.Products.list());
	}
	public static Result delete(Product product) {
		if(product == null) {
			return notFound(String.format("Product %s does not exists.", product.ean));
		}
		Product.remove(product);
		return redirect(routes.Products.list());
	}
}
